package com.jonkryl.lenaapps.network;

import com.jonkryl.lenaapps.model.Hits;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET("api/?key=14024470-ca0908ee229ab4e55aff89b3d&orientation=vertical")
    Call<Hits> getImages(@Query("page") int page, @Query("q") String q);
}
