package com.jonkryl.lenaapps.network;

import com.jonkryl.lenaapps.model.Image;

import java.util.List;

public interface LoadingListener {
    void complete(List<Image> images, int page, boolean update);
}
