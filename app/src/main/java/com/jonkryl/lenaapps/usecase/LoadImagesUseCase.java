package com.jonkryl.lenaapps.usecase;

import com.jonkryl.lenaapps.Constant;
import com.jonkryl.lenaapps.model.Hits;
import com.jonkryl.lenaapps.network.LoadingListener;
import com.jonkryl.lenaapps.network.Network;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadImagesUseCase {
    private final LoadingListener loadingIsComplete;
    private final Network network;

    public LoadImagesUseCase(LoadingListener loadingIsComplete, Network network) {
        this.loadingIsComplete = loadingIsComplete;
        this.network = network;
    }

    public void loadImages(int page, String q, final boolean update) {
        if (page < Constant.DEFAULT_PAGE) {
            page = Constant.DEFAULT_PAGE;
        }
        final int finalPage = page;
        network.getApiService().getImages(page, q).enqueue(new Callback<Hits>() {
            @Override
            public void onResponse(Call<Hits> call, Response<Hits> response) {
                if (response.body() != null) {
                    loadingIsComplete.complete(response.body().getImages(), finalPage, update);
                }
            }

            @Override
            public void onFailure(Call<Hits> call, Throwable t) {
            }
        });
    }
}
