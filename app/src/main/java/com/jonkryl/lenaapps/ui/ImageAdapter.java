package com.jonkryl.lenaapps.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.jonkryl.lenaapps.Constant;
import com.jonkryl.lenaapps.R;
import com.jonkryl.lenaapps.model.Image;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends BaseAdapter {
    private final static int GRID_SIZE = 500;
    private final Context mContext;
    private List<Image> images = new ArrayList<>();
    private int page = Constant.DEFAULT_PAGE;

    ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return images.size();
    }

    public List<Image> getImages() {
        return images;
    }

    public Object getItem(int position) {
        return images.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        if (convertView == null) {
            imageView.setLayoutParams(new GridView.LayoutParams(GRID_SIZE, GRID_SIZE));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }
        Picasso.with(mContext)
                .load(images.get(position).getPreviewURL())
                .error(R.mipmap.ic_launcher)
                .into(imageView);
        return imageView;
    }

    public void setItem(List<Image> images, int page, boolean update) {
        if (page != this.page) {
            this.page = page;
            this.images.addAll(images);
        } else {
            if (this.images.size() < images.size()) {
                this.images = images;
            }
        }
        if (update) {
            this.images = images;
        }
    }
}
