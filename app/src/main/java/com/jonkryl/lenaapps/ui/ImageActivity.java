package com.jonkryl.lenaapps.ui;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.navigation.NavigationView;
import com.jonkryl.lenaapps.Constant;
import com.jonkryl.lenaapps.PicassoTrustAll;
import com.jonkryl.lenaapps.R;
import com.jonkryl.lenaapps.model.Image;
import com.jonkryl.lenaapps.network.LoadingListener;
import com.jonkryl.lenaapps.network.Network;
import com.jonkryl.lenaapps.usecase.LoadImagesUseCase;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.List;

public class ImageActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private final static int FULL_IMAGE = 20;
    private final static int LOW_IMAGE = 19;

    private final ImageAdapter imageAdapter = new ImageAdapter(this);
    private final LoadImagesUseCase updateListImage = new LoadImagesUseCase(new LoadingIsComplete(), new Network());
    private String q;
    private GridView gridview;

    public class LoadingIsComplete implements LoadingListener {
        @Override
        public void complete(List<Image> images, int page, boolean update) {
            imageAdapter.setItem(images, page, update);
            imageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateListImage.loadImages(Constant.DEFAULT_PAGE, null, true);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        gridview = findViewById(R.id.gridView);
        gridview.setAdapter(imageAdapter);

        gridview.setOnScrollListener(gridViewOnScrollListener);

        gridview.setOnItemClickListener(gridViewOnItemClickListener);
        new AppBarConfiguration.Builder(
                R.id.b1, R.id.b2, R.id.b3,
                R.id.b4, R.id.b5)
                .setDrawerLayout(drawer)
                .build();
    }


    private final GridView.OnScrollListener gridViewOnScrollListener = new GridView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (totalItemCount - firstVisibleItem < LOW_IMAGE) {
                updateListImage.loadImages(totalItemCount / FULL_IMAGE + Constant.DEFAULT_PAGE, q, false);
            }
        }
    };

    private final GridView.OnItemClickListener gridViewOnItemClickListener = new GridView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {
            loadImage(ImageActivity.this, imageAdapter.getImages().get(position).getLargeImageURL());
        }
    };

    private void loadImage(final Context ctx, final String url) {
        PicassoTrustAll.getInstance(ctx)
                .load(url)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        WallpaperManager myWallpaperManager = WallpaperManager
                                .getInstance(getApplicationContext());
                        try {
                            myWallpaperManager
                                    .setBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(ImageActivity.this, "Готово", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        Log.d("WallpaperManager", "onBitmapFailed");

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        Log.d("WallpaperManager", "onPrepareLoad");
                        Toast.makeText(ImageActivity.this, "Загрузка...", Toast.LENGTH_SHORT).show();
                    }
                });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        getNewListImages(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        getNewListImages(menuItem.getTitle().toString());
        return false;
    }

    private void getNewListImages(String q) {
        this.q = q;
        updateListImage.loadImages(Constant.DEFAULT_PAGE, q, true);
        gridview.setSelection(0);
    }
}
