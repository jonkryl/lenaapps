package com.jonkryl.lenaapps.model;

public class Image {
    private String previewURL, largeImageURL;

    public String getPreviewURL() {
        return previewURL;
    }

    public String getLargeImageURL() {
        return largeImageURL;
    }
}
