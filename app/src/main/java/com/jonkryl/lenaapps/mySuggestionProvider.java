package com.jonkryl.lenaapps;

import android.content.SearchRecentSuggestionsProvider;

public class mySuggestionProvider extends SearchRecentSuggestionsProvider {
    private final static String AUTHORITY = "com.jonkryl.lenaapps.mySuggestionProvider";
    private final static int MODE = DATABASE_MODE_QUERIES;

    public mySuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
